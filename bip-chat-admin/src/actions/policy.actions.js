import { policyConstants } from '../constants';
import { policyService } from '../services';

export const policyActions = {
    toggleValidation,
    //getTransactions
};

function toggleValidation(id, newStatus) {

    return dispatch => {
        dispatch(request());

        policyService.toggleValidation(id, newStatus)
            .then(
            result => {
                dispatch(success(result.status, result.userData.id, result.userData.policy_invalid));
            },
            error => {
                dispatch(failure(error));
            });
    }


    function request() {
        return {
            type: policyConstants.POLICY_CHANGE_REQUEST
        }
    }

    function success(status, id, policy_invalid) {
        return {
            type: policyConstants.POLICY_CHANGE_SUCCESS,
            status,
            id,
            policy_invalid
        }
    }

    function failure(error) {
        return {
            type: policyConstants.POLICY_CHANGE_ERROR,
            error
        }
    }

}