import { usersConstants } from '../constants';
import { usersService } from '../services';

export const usersActions = {
    getUsers,
    //getTransactions
};

function getUsers() {

    return dispatch => {
        dispatch(request());

        usersService.getUsers()
            .then(
            users => {
                dispatch(success(users));
            },
            error => {
                dispatch(failure(error));
            });
    }


    function request() {
        return {
            type: usersConstants.USER_REQUEST
        }
    }

    function success(users) {
        return {
            type: usersConstants.USER_SUCCESS,
            users
        }
    }

    function failure(error) {
        return {
            type: usersConstants.USER_ERROR,
            error
        }
    }

}

// function getTransactions(token) {

//     return dispatch => {
//         dispatch(request({
//             token
//         }));

//         dashboardService.getTransactions(token)
//             .then(
//             transactions => {
//                 dispatch(success(transactions));
//             },
//             error => {
//                 dispatch(failure(error));
//             });
//     }


//     function request(token) {
//         return {
//             type: dashboardConstants.TRANSACTION_HISTORY_REQUEST,
//             token
//         }
//     }

//     function success(transactions) {
//         return {
//             type: dashboardConstants.TRANSACTION_HISTORY_SUCCESS,
//             transactions
//         }
//     }

//     function failure(error) {
//         return {
//             type: dashboardConstants.TRANSACTION_HISTORY_ERROR,
//             error
//         }
//     }
// }