import React, { Component } from 'react';

import { connect } from 'react-redux';
import { usersActions } from '../../actions';

import './App.css';

import { ValidateButton } from '../Shared/ValidateButton/ValidateButton.jsx';
import { ModalImage } from '../Shared/ModalImage/ModalImage.jsx';
import { PoliceValidateButton } from '../Shared/PoliceValidateButton/PoliceValidateButton.jsx';

// const url = "http://dgate.devhost.io:3001/api/list_users";
const imageUrl = "http://dgate.devhost.io:3001/";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      error: ''
    }
  }

  componentWillMount() {
    const { getUsers } = this.props;
    getUsers();
  }

  render() {
    const { users } = this.props;
    const { items } = users;
    return (
      <div>
        <table className="table table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Car ID</th>
              <th>Phone</th>
              <th>Policy</th>
              <th>Policy validate</th>
              <th>Due to</th>
              <th>Car number</th>
              <th>Car policy</th>
              <th>Validate</th>
            </tr>
          </thead>
          <tbody>
            {items && items.map(item => (
              <tr key={item.id}>
                <td>{item.username}</td>
                <td>{item.car_number}</td>
                <td>{item.phone}</td>
                <td>{item.car_policy_number}</td>
                <td>
                  <PoliceValidateButton user_id={item.id} status={item.policy_invalid} />
                </td>
                <td></td>
                <td>
                  <ModalImage imageUrl={imageUrl + item.photo_car_number} />
                </td>
                <td>
                  <ModalImage imageUrl={imageUrl + item.photo_car_policy} />
                </td>
                <td>
                  <ValidateButton user_id={item.id} status={item.status} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

const connectedApp = connect(state => {
  const { users } = state;
  return {
    users
  };
}, { getUsers: usersActions.getUsers })(App);

export { connectedApp as App };
