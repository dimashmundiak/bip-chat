import React, { Component } from 'react';
import './PoliceValidateButton.css';

import { connect } from 'react-redux';

import { policyActions } from '../../../actions';

class PoliceValidateButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 0
        }
        this.toggle = this.toggle.bind(this);
    }

    componentWillMount() {
        const { status } = this.props;
        this.setState({
            status: status
        });
    }

    componentWillReceiveProps(nextProps) {
        const { validation, user_id } = nextProps;
        if (validation.result && user_id === validation.id) {
            this.setState({
                status: validation.policy
            })
        }
    }

    toggle() {
        const { toggleValidation, user_id } = this.props;
        const { status } = this.state;
        let newStatus = (+status === 1) ? -1 : 1;
        toggleValidation(user_id, newStatus);
    }

    render() {
        const { status } = this.state;
        var button = null;
        if (+status === 1) {
            button = <div>
                <button className="btn btn-success" onClick={this.toggle}>Valid</button>
            </div>
        } else {
            button = <div>
                <button className="btn btn-danger" onClick={this.toggle}>Invalid</button>
            </div>
        }
        return (
            <div>{button}</div>
        )
    }
}

const connectedPoliceValidateButton = connect(state => {
    const { validation } = state;
    return {
        validation
    };
}, { toggleValidation: policyActions.toggleValidation })(PoliceValidateButton);

export { connectedPoliceValidateButton as PoliceValidateButton };
