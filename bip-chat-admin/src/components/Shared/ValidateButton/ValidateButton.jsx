import React, { Component } from 'react';
import './ValidateButton.css';


const url = 'http://dgate.devhost.io:3001/api/update_user_status';

class ValidateButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 0
        }
        this.rejectValidation = this.rejectValidation.bind(this);
        this.acceptValidation = this.acceptValidation.bind(this);
        this.toggleValidation = this.toggleValidation.bind(this);
    }

    componentWillMount() {
        const { status } = this.props;
        this.setState({
            status: status
        });
    }



    rejectValidation() {
        let that = this;
        const { user_id } = this.props;
        fetch(url,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ user_id: user_id, status: -1 })
            })
            .then(function (res) {
                that.setState({
                    status: -1
                });
            })
            .catch(function (res) {
                console.log(res)
            })
    }

    acceptValidation() {
        let that = this;
        const { user_id } = this.props;
        fetch(url,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ user_id: user_id, status: 1 })
            })
            .then(function (res) {
                that.setState({
                    status: 1
                });
            })
            .catch(function (res) {
                console.log(res)
            })
    }

    toggleValidation() {
        let that = this;
        const { user_id } = this.props;
        const { status } = this.state;
        let newStatus = (+status === 1) ? -1 : 1;
        fetch(url,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ user_id: user_id, status: newStatus })
            })
            .then(res => {
                that.setState({
                    status: newStatus
                });
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        const { status } = this.state;
        var button = null;
        if (+status === 0) {
            button = <div>
                <button className="btn btn-danger" onClick={this.rejectValidation}>Reject</button>
                <button className="btn btn-success" onClick={this.acceptValidation}>Accept</button>
            </div>
        } else if (+status === 1) {
            button = <div>
                <button className="btn btn-success" onClick={this.toggleValidation}>Accepted</button>
            </div>
        } else {
            button = <div>
                <button className="btn btn-danger" onClick={this.toggleValidation}>Rejected</button>
            </div>
        }
        return (
            <div>{button}</div>
        )
    }
}

export { ValidateButton };
