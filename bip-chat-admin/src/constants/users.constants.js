export const usersConstants = {
    USER_REQUEST: 'USER_REQUEST',
    USER_SUCCESS: 'USER_SUCCESS',
    USER_ERROR: 'USER_ERROR'
}