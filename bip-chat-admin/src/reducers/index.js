import { combineReducers } from 'redux';
import { users } from './users.reducer';
import { validation } from './policy.reducer';

const rootReducer = combineReducers({
  users,
  validation
});

export default rootReducer;