import { policyConstants } from '../constants';

const initialState = {};

export function validation(state = initialState, action) {
    switch (action.type) {
        case policyConstants.POLICY_CHANGE_REQUEST:
            return {
                ...state,
            };
        case policyConstants.POLICY_CHANGE_SUCCESS:
            return {
                result: action.status,
                id: action.id,
                policy: action.policy_invalid
            };
        case policyConstants.POLICY_CHANGE_ERROR:
            return {
                ...state
            };
        default:
            return state
    }
}
