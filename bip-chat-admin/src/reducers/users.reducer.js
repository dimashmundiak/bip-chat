import { usersConstants } from '../constants';

const initialState = [];

export function users(state = initialState, action) {
    switch (action.type) {
        case usersConstants.USER_REQUEST:
            return {
                ...state,
            };
        case usersConstants.USER_SUCCESS:
            return {
                items: action.users
            };
        case usersConstants.USER_ERROR:
            return {
                ...state
            };
        default:
            return state
    }
}
