import axios from 'axios';

export const policyService = {
    toggleValidation
};

function toggleValidation(id, newStatus) {

    return new Promise((resolve, reject) => {
        axios.post('http://dgate.devhost.io:3001/api/update_user_policy_invalid', { user_id: id, policy_invalid: newStatus })
            .then(function (response) {
                const {
                    status, userData
                } = response.data;
                resolve({ status, userData });
            })
            .catch(function (error) {
                const {
                        errorMessage
                    } = error;
                reject(errorMessage);
            });
    })
}