import axios from 'axios';

export const usersService = {
    getUsers
};

function getUsers() {
    var config = {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    };
    return new Promise((resolve, reject) => {
        axios.get('http://dgate.devhost.io:3001/api/list_users', config)
            .then(function (response) {
                const {
                    listUsers
                } = response.data;
                resolve(listUsers);
            })
            .catch(function (error) {
                const {
                        errorMessage
                    } = error;
                reject(errorMessage);
            });
    })
}